﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Telefon_Rehberii.Models;

namespace Telefon_Rehberii.Controllers
{
    public class calisansController : Controller
    {
        private telefonEntities db = new telefonEntities();

        // GET: calisans
        public ActionResult Index()
        {
            var calisans = db.calisans.Include(c => c.departman);
            return View(calisans.ToList());
        }

        // GET: calisans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            calisan calisan = db.calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            return View(calisan);
        }

        // GET: calisans/Create
        public ActionResult Create()
        {
            ViewBag.departmanid = new SelectList(db.departmen, "id", "departman_adi");
            return View();
        }

        // POST: calisans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,ad,soyad,telefon,departmanid,yoneticiid")] calisan calisan)
        {
            if (ModelState.IsValid)
            {
                db.calisans.Add(calisan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.departmanid = new SelectList(db.departmen, "id", "departman_adi", calisan.departmanid);
            return View(calisan);
        }

        // GET: calisans/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            calisan calisan = db.calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            ViewBag.departmanid = new SelectList(db.departmen, "id", "departman_adi", calisan.departmanid);
            return View(calisan);
        }

        // POST: calisans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,ad,soyad,telefon,departmanid,yoneticiid")] calisan calisan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(calisan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.departmanid = new SelectList(db.departmen, "id", "departman_adi", calisan.departmanid);
            return View(calisan);
        }

        // GET: calisans/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            calisan calisan = db.calisans.Find(id);
            if (calisan == null)
            {
                return HttpNotFound();
            }
            return View(calisan);
        }

        // POST: calisans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            calisan calisan = db.calisans.Find(id);
            db.calisans.Remove(calisan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
