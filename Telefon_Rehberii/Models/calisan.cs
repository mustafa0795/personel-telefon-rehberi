//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Telefon_Rehberii.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class calisan
    {
        public int id { get; set; }
        public string ad { get; set; }
        public string soyad { get; set; }
        public string telefon { get; set; }
        public Nullable<int> departmanid { get; set; }
        public Nullable<int> yoneticiid { get; set; }
    
        public virtual departman departman { get; set; }
    }
}
