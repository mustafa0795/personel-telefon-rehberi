﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Telefon_Rehberii.Startup))]
namespace Telefon_Rehberii
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
